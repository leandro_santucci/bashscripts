#!/bin/bash
clear

UBUNTU_VERSION=$(lsb_release -s --codename)

WEB_BROWSER=(
	"Brave" "deb https://brave-browser-apt-release.s3.brave.com $UBUNTU_VERSION main;https://brave-browser-apt-release.s3.brave.com/brave-core.asc" "brave" "brave-browser"
	"Chromium" "ppa:chromium-team/stable" "chromium" "chromium-browser"
	"Google Chrome" "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main;https://dl-ssl.google.com/linux/linux_signing_key.pub" "google-chrome" "google-chrome-stable"
	"Mozilla Firefox" "ppa:mozillateam/firefox-next" "firefox" "firefox"
	"Opera" "deb http://deb.opera.com/opera-stable/ stable non-free;http://deb.opera.com/archive.key" "opera" "opera-stable"
	"Vivaldi" "deb [arch=i386,amd64] http://repo.vivaldi.com/stable/deb/ stable main;http://repo.vivaldi.com/stable/linux_signing_key.pub" "vivaldi-stable" "vivaldi-stable"		
	)

EMAIL_CLIENT=(
	"Evolution" "urepo" "urepo" "evolution"
	"Geary" "ppa:geary-team/releases" "geary" "geary"
	"Mailspring" "https://updates.getmailspring.com/download?platform=linuxDeb" "indefinido" "mailspring"
	"Thunderbird" "ppa:mozillateam/thunderbird-next" "thunderbird" "thunderbird"
	)

FILE_MANAGER=(
	"Thunar" "urepo" "urepo" "thunar"
	"Dolphin" "urepo" "urepo" "dolphin"
	"Nautilus" "urepo" "urepo" "nautilus"
	"Nemo" "urepo" "urepo" "nemo"
	"Ranger" "urepo" "urepo" "ranger"
	)

TERMINAL_EMULATOR=(
	"Gnome Terminal" "urepo" "urepo" "gnome-terminal"
	"Terminator" "urepo" "urepo" "terminator"
	"Tilix" "ppa:webupd8team/terminix" "webupd8team-tilix" "tilix"
	"Xfce Terminal" "urepo" "urepo" "xfce4-terminal"
	)

MEDIA_PLAYER=(
	"MPV" "ppa:mc3man/mpv-tests" "mpv" "mpv"
	"VLC" "urepo" "urepo" "vlc"
	"Parole" "urepo" "urepo" "parole"
	"SMPlayer" "ppa:rvm/smplayer" "smplayer" "smplayer"
	)

AUDIO_PLAYER=(
	"Amarok" "urepo" "urepo" "amarok"
	"Banshee" "urepo" "urepo" "banshee"
	"Clementine" "urepo" "urepo" "clementine"
	"Gnome Music" "urepo" "urepo" "gnome-music"
	"Rhythmbox" "urepo" "urepo" "rhythmbox"
	)

IMAGE_EDITOR=(
	"Gimp" "ppa:otto-kesselgulasch/gimp" "otto-gimp" "gimp gimp-gmic gmic"
	"Krita" "ppa:kritalime/ppa" "krita" "krita"
	"My Paint" "urepo" "urepo" "mypaint"																						
	"Pinta" "urepo" "urepo" "pinta"
	)

IMAGE_VIEWER=(
	"Ristretto" "urepo" "urepo" "ristretto"
	"Mirage" "urepo" "urepo" "mirage"
	"Nomacs" "urepo" "urepo" "nomacs"
	"Gwenview" "urepo" "urepo" "gwenview"
	"Eye of Gnome" "urepo" "urepo" "eog"
	)

ARCHIVE_MANAGER=(
	"File Roller" "urepo" "urepo" "file-roller"
	"Engrampa" "urepo" "urepo" "engrampa"
	"Ark" "urepo" "ark" "ark"
	)
# "http://download.documentfoundation.org/libreoffice/stable/VERSION/deb/x86_64/LibreOffice_VERSION_Linux_x86-64_deb_langpack_LANGUAGE.tar.gz"
# Name / MAIN URL / Return Version to insert in URL / LANGUAGE URL
OFFICE_SUITE=(
	"Libre Office" "http://download.documentfoundation.org/libreoffice/stable/VERSION/deb/x86_64/LibreOffice_VERSION_Linux_x86-64_deb.tar.gz" "curl -sL https://download.documentfoundation.org/libreoffice/stable/ | xargs -0 | egrep -o '/\">.*</a></td>' | tail -1 | tr -d '/\"><atd'" "indefinido"
	"Only Office" "https://download.onlyoffice.com/install/desktop/editors/linux/onlyoffice-desktopeditors_amd64.deb" "indefinido" "onlyoffice"
	"WPS Office" "http://kdl.cc.ksosoft.com/wps-community/download/6757/wps-office_10.1.0.6757_amd64.deb" "indefinido" "wpsoffice"
	)

TASK_MANAGER=(
	"Htop" "urepo" "urepo" "htop"
	"Gnome System M." "urepo" "gnome-system-monitor" "gnome-system-monitor"
	"Xfce Task Manager" "urepo" "xfce4-taskmanager" "xfce4-taskmanager"
	"KsysGuard" "urepo" "ksysguard" "ksysguard"
	"glances" "urepo" "glances" "glances"
	)
# WEB_BROWSER EMAIL_CLIENT FILE_MANAGER TERMINAL_EMULATOR MEDIA_PLAYER AUDIO_PLAYER IMAGE_EDITOR IMAGE_VIEWER ARCHIVE_MANAGER TASK_MANAGER
OPTIONS=( OFFICE_SUITE )

echo 

declare -a choices=()

for (( i = 0; i < ${#OPTIONS[@]}; i++ )); do
	declare -n arr="${OPTIONS[i]}"
	title="Applications Installer"
	text=$( echo "Select the <b>${OPTIONS[i]}</b>:" | tr "_" " " )
	height=200
	if [[ $((${#arr[@]} / 4)) -gt 4 ]]; then
		height=$(( $((${#arr[@]} / 4 - 4)) * 21 + 200 ))
	fi

 	num=$(for (( j=0; j<${#arr[@]}; j+=4 )); do
   		echo "$j" 
    	echo "${arr[j]}"
      	if [[ ${arr[j+1]:0:4} = "urep" ]]; then
      		echo "Ubuntu"
      	elif [[ ${arr[j+1]:0:4} = "http" ]]; then
      		echo "Website"
        elif [[ ${arr[j+1]:0:3} = "ppa" || ${arr[j+1]:0:3} = "deb" ]]; then
          	echo "Oficial"
        fi

        if [[ $(dpkg -l | grep "\\<${arr[j+3]}\>") ]]; then
        	echo "installed"
        else
        	echo "not installed"
        fi 	 
      	done | zenity --list --hide-column=1 --text="$text" --cancel-label="SKIP" --width="325" --height="$height" --title="$title" --column " " --column="Application" --column "Repository" --column "Status" 2>/dev/null )
    
    if [[ ! -z $num ]]; then
    	echo "Item Selected: ${arr[num]}"
    	for (( x = 0; x < 4; x++ )); do
    		choices+=( "${arr[num+x]}" )
    	done
    fi
done

clear

if [[ ! -z $choices ]]; then
	echo -n "Your choices: "
	for (( i = 0; i < ${#choices[@]}; i+=4 )); do
		echo -n " ${choices[i]}"
	done	
	echo -e "\n"
fi

for (( i = 0; i < ${#choices[@]}; i+=4 )); do
	# Ubuntu Repository
	if [[ "${choices[i+1]:0:1}" = "u" ]]; then
		applications+="${choices[i+3]} "
	# External Repository
	elif [[ "${choices[i+1]:0:1}" = "d" ]]; then
 		echo "Adding External Repository for ${choices[i]}: $(echo ${choices[i+1]} | cut -d ";" -f 1)"
 		if [[ -f "/etc/apt/sources.list.d/${choices[i+2]}.list" ]]; then
 			echo "Repository already exists."
 			applications+="${choices[i+3]}"
 		else
 		curl -s $(echo ${choices[i+1]} | cut -d ";" -f 2) | sudo apt-key add -
 		sudo bash -c "echo -e '$(echo ${choices[i+1]} | cut -d ";" -f 1)' > /etc/apt/sources.list.d/${choices[i+2]}.list"
 		applications+="${choices[i+3]} "
 		fi
	# External Repository Via PPA
	elif [[ "${choices[i+1]:0:1}" = "p"  ]]; then
		sudo add-apt-repository ${choices[i+1]}
		applications+="${choices[i+3]} "

	# Website
	elif [[ "${choices[i+1]:0:4}" = "http" ]]; then
		
		#echo "TAMANHO: ${#choices[i+2]} = ${choices[i+2]:$len-2:$len} "

		#Get Version
		if [[ "${choices[i+2]:0:4}" = "curl" ]]; then
			echo "ENTROU"
			echo "Finding Lastest version of ${choices[i]}"
			version=$(eval "${choices[i+2]}")
			echo "Lastest Version:" $version
			choices[i+1]=${choices[i+1]//VERSION/$version}
			#echo ${choices[i+1]}

			if [[ ${choices[i+3]:0:4} = "http" ]]; then
				choices[i+3]=${choices[i+3]//VERSION/$version}

			fi
		fi
		len=${#choices[i+1]}
		if [[ "${choices[i+1]:$len-2:$len}" = "eb" ]]; then
			echo "Downloading ${choices[i]}..."
			wget -q "${choices[i+1]}"  --output-document=${choices[i+3]}.deb	
			if [[ $? -ne 0 ]]; then
				echo "Ocorreu um erro ao baixar."
			else
				echo "Baixado com sucesso!"
			fi
				echo "Installing ${choices[i]}..."
				sudo gdebi ${choices[i+3]}.deb
		fi

		if [[ "${choices[i+1]:$len-6:$len}" = "tar.gz" ]]; then
			echo "Downloading ${choices[i]}..."
			output=$("${choices[i]}" | tr " " "-")
			echo "saida: "$output
			output=$(echo "${choices[i]}" | tr " " "-")
			wget "${choices[i+1]}"  --output-document=$output.tar.gz	
			if [[ $? -ne 0 ]]; then
				echo "Ocorreu um erro ao baixar."
			else
				mkdir $output
				echo "Success Download"
				echo "Unpacking..."
				#tar -xvzf $output.tar.gz ./$output
				tar -xvf $output.tar.gz -C $output --wildcards --no-anchored '*.deb' --strip-components=2
				echo "Installing ${choices[i]}..."
				sudo dpkg -i ./$output/*.deb
			fi
				#sudo gdebi ${choices[i+3]}.deb
		fi

		# Select Language:
		if [[ "${choices[i]}" = "${OFFICE_SUITE[0]}" ]]; then
			language=$(zenity --list --column "Language" "en-US" "pt-BR")
			file_url=${choices[i+1]//.tar.gz/_langpack_$language.tar.gz}
			#echo "$file_url"
			wget "$file_url"  --output-document=$output-$language.tar.gz
			echo "Unpacking..."
			mkdir $output-$language
			tar -xvf $output-$language.tar.gz -C $output-$language --wildcards --no-anchored '*.deb' --strip-components=2
			sudo dpkg -i ./$output-$language/*.deb
		#	http://download.documentfoundation.org/libreoffice/stable/6.2.0/deb/x86_64/LibreOffice_6.2.0_Linux_x86-64_deb_langpack_pt-BR.tar.gz
		fi
	fi

done

if [[ "${#applications[@]}" -ge 1 ]]; then
	sudo apt-get update
	echo "Installing packages from repository..."
	sudo apt-get install ${applications[@]} -y
fi

# url=$(curl -sL https://www.libreoffice.org/download/download/ | xargs -0 | egrep -o "deb-x86_64&version.*?&lang=" | head -1)
# url=$(curl -sL https://download.documentfoundation.org/libreoffice/stable/ | xargs -0 | egrep -o "/\">.*</a></td>" | tail -1) -- VERSION
# url=$(curl -sL https://download.documentfoundation.org/libreoffice/stable/ | xargs -0 | egrep -o '/\">.*</a></td>'' | tail -1 | tr -d '/\"><atd') -- ONLY VERSION


