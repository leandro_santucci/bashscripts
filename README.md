# BashScripts

Scripts criados para facilitar procedimentos longos para instalações/configurações de aplicações e customizações de interface após a instalação de uma distribuição Linux.

### Scripts

##### theme-pack (xfce)
Script para customização do tema do ambiente gráfico XFCE, contendo:
- Pacote de Icones Suru++ [[Link]](https://github.com/gusbemacbe/suru-plus)
- Tema X-Arc-White [[Link]](https://gitlab.com/LinxGem33/X-Arc-White)
- Docker Plank
- Plano de fundo 
- Configuração para o Whisker Menu

<div align="center">
<img align="center" src="https://drive.google.com/uc?id=1XE9ulmQSCyGSWm924LyobSuXDfJMBkPf">
</div>

### Como usar?
Após clonar o repisotório, utilize os seguintes comandos:

```
$ chmod +X ./bashscripts/NOME_SCRIPT.sh
$ bash ./bashscripts/NOME_SCRIPT.sh
````

### Requerimentos
- Distribuições baseadas em Debian/Ubuntu
- Ambiente gráfico exigente conforme as necessidades do script