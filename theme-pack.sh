#!/bin/bash
# Script para instalação e modificação do tema padrão do sistema.

#Necessário para utilizar o xfconf-query
PID=$(pgrep xfce4-session)
export DBUS_SESSION_BUS_ADDRESS=$(grep -z DBUS_SESSION_BUS_ADDRESS /proc/$PID/environ|cut -d= -f2-) &&

sudo apt-get install git wget plank &&
#remover Diretórios
echo -e "\e[34m\e[1m(??)\e[0m Buscando e removendo icones Suru++..." &&

if [ -d "/usr/share/icons/Suru++" ] ; then
	sudo rm -r /usr/share/icons/Suru++ &&
	echo -e  "\e[31m\e[1m(--)\e[0m Remoção Finalizada"
fi

echo -e "\e[34m\e[1m(??)\e[0m Buscando e removendo tema X-Arc-White"
if [ -d "/usr/share/themes/X-Arc-White" ] ; then
	sudo rm -r /usr/share/themes/X-Arc-White &&
	echo -e "\e[31m\e[1m(--)\e[0m Remoção Finalizada"
fi

echo -e "\e[1mBaixando/Atualizando tema de icones Suru++...\e[0m" &&
if [ ! -d "suru-plus/" ] ; then
    git clone https://github.com/gusbemacbe/suru-plus
else
    cd "suru-plus/" &&
    git pull https://github.com/gusbemacbe/suru-plus &&
    cd ..
fi

echo -e "\e[1mBaixando/Atualizando tema de janelas X-Arc-White...\e[0m" &&
if [ ! -d "X-Arc-White/" ] ; then
    git clone https://gitlab.com/LinxGem33/X-Arc-White
else
	cd "X-Arc-White/" &&
	git pull https://gitlab.com/LinxGem33/X-Arc-White &&
	cd ..
fi

echo "Criando Symbolic Link do Plank para AutoStart..." &&
if [ ! -d "$HOME/.config/autostart" ] ; then
	mkdir $HOME/.config/autostart
fi

if [ ! -f "$HOME/.config/autostart/plank.desktop" ] ; then
    ln -s /usr/share/applications/plank.desktop $HOME/.config/autostart/plank.desktop
fi

echo "Baixando e alocando Wallpaper..."
if [ ! -d "$HOME/wallpapers" ] ; then
	mkdir $HOME/wallpapers
fi

if [ ! -f "$HOME/wallpapers/blue-sky-mountains.jpg" ] ; then
	echo -e "Baixando Wallpaper" &&
	wget --output-document="blue-sky-mountains.jpg" "https://images.pexels.com/photos/1054218/pexels-photo-1054218.jpeg" &&
	mv -f blue-sky-mountains.jpg $HOME/wallpapers
fi

## --> Aplicando temas
echo -e "\e[1mAplicando Temas...\e[0m" &&
sudo mv -i X-Arc-White/ /usr/share/themes/  &&
sudo mv -i suru-plus/Suru++/ /usr/share/icons/ &&
sudo gtk-update-icon-cache /usr/share/icons/Suru++ &&
sudo rm -r suru-plus/ &&

# Tema LightDM
sudo bash -c "echo -e '[greeter]\nbackground = $HOME/wallpapers/blue-sky-mountains.jpg\ntheme-name = X-Arc-White\nicon-theme-name = Suru++' > /etc/lightdm/lightdm-gtk-greeter.conf"

## GNOME SHELL (GTK APPLICATION)
gsettings set org.gnome.desktop.interface gtk-theme "X-Arc-White" &&
gsettings set org.gnome.desktop.wm.preferences theme "X-Arc-White" &&
gsettings set org.gnome.desktop.interface icon-theme "Suru++" &&

## XFCE4 
xfconf-query -c xsettings -p /Net/IconThemeName -s "Suru++" &&
xfconf-query -c xsettings -p /Net/ThemeName -s "X-Arc-White" &&
xfconf-query -c xfwm4 -p /general/theme -s "X-Arc-White" &&
xfconf-query -c xfwm4 -p /general/placement_mode -s "center" &&
xfconf-query -c xfwm4 -p /general/button_layout -s "CHM|O" &&
xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitor0/workspace0/last-image -s "$HOME/wallpapers/blue-sky-mountains.jpg"

configs=( "show-button-icon" "false"
 "button-title" "\<span font=\"thin\"\> Applications \<\/span\>" 
  "show-button-title" "true" 
  "position-categories-alternate" "true" 
  "category-icon-size" "0" 
  "item-icon-size" "2" )

for i in {0..10..2}; do   
    sed -i "s/\(${configs[i]} *= *\).*/\1${configs[i+1]}/" $HOME/.config/xfce4/panel/whiskermenu-1.rc  
done

unset configs &&

xfce4-panel -r &&
#nohup
plank > /dev/null &

# sed -c -i "s/\($TARGET_KEY *= *\).*/\1$REPLACEMENT_VALUE/" $CONFIG_FILE

echo -e "\e[32m\e[1mConcluido!"